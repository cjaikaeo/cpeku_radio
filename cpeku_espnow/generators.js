Blockly.JavaScript['cpeku_espnow_enable'] = function(block) {
  var number_channel = block.getFieldValue('channel');
  var number_group = block.getFieldValue('group');
  var code = 'DEV_IO.CPEKU_EspNow().enable(' 
           + number_channel
           + ',' + number_group
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_espnow_on_message'] = function(block) {
  var text_topic = block.getFieldValue('topic');
  var statements_handler = Blockly.JavaScript.statementToCode(block, 'handler');
  var code = 'DEV_IO.CPEKU_EspNow().on_message(' 
           + '"' + text_topic + '"'
           + ',[]() {\n'
           + statements_handler
           + '});\n';
  return code;
};

Blockly.JavaScript['cpeku_espnow_message_num'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().received_number()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_message_text'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().received_text()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_my_addr'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().my_address()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_my_group'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().my_group()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_sender_addr'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().sender_address()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_sender_group'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().sender_group()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_broadcast'] = function(block) {
  var text_topic = block.getFieldValue('topic');
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'DEV_IO.CPEKU_EspNow().broadcast(' 
           + '"' + text_topic + '"'
           + ',' + value_msg
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_espnow_broadcast_group'] = function(block) {
  var text_topic = block.getFieldValue('topic');
  var value_group = Blockly.JavaScript.valueToCode(block, 'group', Blockly.JavaScript.ORDER_ATOMIC);
  var value_msg = Blockly.JavaScript.valueToCode(block, 'msg', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'DEV_IO.CPEKU_EspNow().broadcast(' 
           + '"' + text_topic + '"'
           + ',' + value_msg
           + ', (uint16_t)' + value_group
           + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_espnow_is_enabled'] = function(block) {
  var code = 'DEV_IO.CPEKU_EspNow().is_enabled()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_espnow_set_group'] = function(block) {
  var value_group = Blockly.JavaScript.valueToCode(block, 'group', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'DEV_IO.CPEKU_EspNow().set_group((uint16_t)' + value_group + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_espnow_set_mode'] = function(block) {
  var dropdown_mode = block.getFieldValue('mode');
  var code = 'DEV_IO.CPEKU_EspNow().set_longrange(' + dropdown_mode + ');\n';
  return code;
};
