#ifndef __CPEKU_ESPNOW_H__
#define __CPEKU_ESPNOW_H__

#include <map>
#include "driver.h"
#include "device.h"
#include "esp_log.h"

class CPEKU_EspNow : public Device {
  public:
    typedef uint16_t group_id_t;
    typedef void(*OnMessageCallback)();
    typedef std::map<std::string,OnMessageCallback> CallbackMap;
    enum State {
      DETECT,
      DISABLED,
      ENABLED,
    };
    struct MsgHeader {
      uint16_t src_group;
      uint16_t dst_group;
      uint8_t  topic_len;
      uint8_t  msg_len;
    };
    CPEKU_EspNow();
    void init(void) override;
    void process(Driver *drv) override;
    int prop_count(void) override { return 0; }
    bool prop_name(int index, char *name) override { return false; }
    bool prop_unit(int index, char *unit) override { return false; }
    bool prop_attr(int index, char *attr) override { return false; }
    bool prop_read(int index, char *value) override { return false; }
    bool prop_write(int index, char *value) override { return false; }

    void enable(uint8_t channel, uint16_t group);
    void on_message(const char* topic, OnMessageCallback cb);
    double received_number();
    char* received_text();
    uint16_t my_group();
    char* my_address();
    uint16_t sender_group();
    char* sender_address();
    void broadcast(const char* topic, const char* msg, uint16_t dst_group);
    void broadcast(const char* topic, const char* msg);
    void broadcast(const char* topic, double num, uint16_t dst_group);
    void broadcast(const char* topic, double num);
    bool is_enabled();
    void set_group(uint16_t group);
    void set_longrange(uint8_t enabled);

  private:
    static State _state;
    static void _recv_cb(const uint8_t *mac_addr, const uint8_t* data, int len);
    static char _received_text[];
    static char _my_mac_str[];
    static uint16_t _my_group;
    static char _sender_mac_str[];
    static uint16_t _sender_group;
    static CallbackMap _cbmap;
};

#endif
