Blockly.Msg.CPEKU_ESPNOW_ENABLE = "enable radio on channel";
Blockly.Msg.CPEKU_ESPNOW_GROUP = "with group";
Blockly.Msg.CPEKU_ESPNOW_ENABLE_TOOLTIP = "enable direct radio communication";

Blockly.Msg.CPEKU_ESPNOW_ON_MSG = "on radio receive topic";
Blockly.Msg.CPEKU_ESPNOW_ON_MSG_TOOLTIP = "perform given commands when receiving a message on the specified topic";

Blockly.Msg.CPEKU_ESPNOW_MSG_NUM = "received message as number";
Blockly.Msg.CPEKU_ESPNOW_MSG_NUM_TOOLTIP = "returns latest received message as number";

Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT = "received message as text";
Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT_TOOLTIP = "returns latest received message as text";

Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR = "sender's address";
Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR_TOOLTIP = "returns latest sender's MAC address";

Blockly.Msg.CPEKU_ESPNOW_BROADCAST = "broadcast radio topic";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_WITH_MSG = "with message";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TOOLTIP = "broadcast message to nearby devices in the same group";

Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED = "radio is enabled";
Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED_TOOLTIP = "check if direct radio communication is enabled";

Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP = "to group";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP_TOOLTIP = "broadcast message to nearby devices in the specified group";

Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP = "sender's group";
Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP_TOOLTIP = "returns latest sender's group number";

Blockly.Msg.CPEKU_ESPNOW_MY_GROUP = "my group";
Blockly.Msg.CPEKU_ESPNOW_MY_GROUP_TOOLTIP = "returns my own group number";

Blockly.Msg.CPEKU_ESPNOW_MY_ADDR = "my address";
Blockly.Msg.CPEKU_ESPNOW_MY_ADDR_TOOLTIP = "returns my own MAC address";

Blockly.Msg.CPEKU_ESPNOW_SET_GROUP = "set radio group to";
Blockly.Msg.CPEKU_ESPNOW_SET_GROUP_TOOLTIP = "set my radio group to the specified number";

Blockly.Msg.CPEKU_ESPNOW_SET_MODE = "set radio mode";
Blockly.Msg.CPEKU_ESPNOW_MODE_STANDARD = "standard";
Blockly.Msg.CPEKU_ESPNOW_MODE_LR = "long-range";
Blockly.Msg.CPEKU_ESPNOW_SET_MODE_TOOLTIP = "set radio mode to standard or long-range";

