Blockly.Msg.CPEKU_ESPNOW_ENABLE = "เปิดใช้งานการสื่อสารตรงบนช่องสัญญาณ";
Blockly.Msg.CPEKU_ESPNOW_GROUP = "เข้าร่วมกลุ่ม";
Blockly.Msg.CPEKU_ESPNOW_ENABLE_TOOLTIP = "เปิดใช้งานการสื่อสารโดยตรงกับอุปกรณ์รอบข้าง";

Blockly.Msg.CPEKU_ESPNOW_ON_MSG = "ทำคำสั่งเมื่อได้รับข่าวในหัวข้อ";
Blockly.Msg.CPEKU_ESPNOW_ON_MSG_TOOLTIP = "ทำคำสั่งที่กำหนดให้เมื่อได้รับข่าวในหัวข้อที่ระบุ";

Blockly.Msg.CPEKU_ESPNOW_MSG_NUM = "ข่าวที่ได้รับล่าสุดในรูปตัวเลข";
Blockly.Msg.CPEKU_ESPNOW_MSG_NUM_TOOLTIP = "คืนค่าข่าวที่ได้รับล่าสุดในรูปตัวเลข";

Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT = "ข่าวที่ได้รับล่าสุดในรูปข้อความ";
Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT_TOOLTIP = "คืนค่าข่าวที่ได้รับล่าสุดในรูปข้อความ";

Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR = "หมายเลขที่อยู่ของผู้ส่ง";
Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR_TOOLTIP = "คืนค่าหมายเลขที่อยู่ของผู้ส่ง";

Blockly.Msg.CPEKU_ESPNOW_BROADCAST = "กระจายข่าวในหัวข้อ";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_WITH_MSG = "ด้วยข้อมูล";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TOOLTIP = "กระจายข่าวให้กับอุปกรณ์รอบข้างที่อยู่ในกลุ่มเดียวกัน";

Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED = "การสื่อสารตรงเปิดใช้งาน";
Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED_TOOLTIP = "ตรวจสอบว่าการสื่อสารตรงมีการเปิดใช้งานหรือไม่";

Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP = "ไปยังกลุ่ม";
Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP_TOOLTIP = "กระจายข่าวให้กับอุปกรณ์รอบข้างที่อยู่ในกลุ่มที่ระบุ";

Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP = "หมายเลขกลุ่มของผู้ส่งข่าว";
Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP_TOOLTIP = "คืนค่าหมายเลขกลุ่มของผู้ส่งข่าวล่าสุด";

Blockly.Msg.CPEKU_ESPNOW_MY_GROUP = "หมายเลขกลุ่มของตน";
Blockly.Msg.CPEKU_ESPNOW_MY_GROUP_TOOLTIP = "คืนค่าหมายเลขกลุ่มของตนเอง";

Blockly.Msg.CPEKU_ESPNOW_MY_ADDR = "หมายเลขที่อยู่ของตน";
Blockly.Msg.CPEKU_ESPNOW_MY_ADDR_TOOLTIP = "คืนค่าหมายเลขที่อยู่ของตน";

Blockly.Msg.CPEKU_ESPNOW_SET_GROUP = "ตั้งค่าหมายเลขกลุ่มเป็น";
Blockly.Msg.CPEKU_ESPNOW_SET_GROUP_TOOLTIP = "ตั้งค่าหมายเลขกลุ่มที่ตนเข้าร่วม";

Blockly.Msg.CPEKU_ESPNOW_SET_MODE = "ตั้งโหมดสื่อสาร";
Blockly.Msg.CPEKU_ESPNOW_MODE_STANDARD = "มาตรฐาน";
Blockly.Msg.CPEKU_ESPNOW_MODE_LR = "ระยะไกล";
Blockly.Msg.CPEKU_ESPNOW_SET_MODE_TOOLTIP = "ตั้งโหมดสื่อสารแบบมาตรฐานหรือระยะไกล";

