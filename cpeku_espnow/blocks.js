Blockly.Blocks['cpeku_espnow_enable'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_ENABLE)
        .appendField(new Blockly.FieldNumber(1, 1, 13, 1), "channel")
        .appendField(Blockly.Msg.CPEKU_ESPNOW_GROUP)
        .appendField(new Blockly.FieldNumber(0, 0, 65535), "group");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_ENABLE_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_on_message'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_ON_MSG)
        .appendField(new Blockly.FieldTextInput("topic"), "topic");
    this.appendStatementInput("handler")
        .setCheck(null);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_ON_MSG_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_message_num'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_MSG_NUM);
    this.setOutput(true, "Number");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_MSG_NUM_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_message_text'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT);
    this.setOutput(true, "String");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_MSG_TEXT_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_sender_addr'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR);
    this.setOutput(true, "String");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_SENDER_ADDR_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_broadcast'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST)
        .appendField(new Blockly.FieldTextInput("topic"), "topic");
    this.appendValueInput("msg")
        .setCheck(["Number", "String"])
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST_WITH_MSG);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_is_enabled'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED);
    this.setOutput(true, "Boolean");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_IS_ENABLED_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_broadcast_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST)
        .appendField(new Blockly.FieldTextInput("topic"), "topic");
    this.appendValueInput("msg")
        .setCheck(["Number", "String"])
        .appendField(new Blockly.FieldNumber(0, 0, 65535), "group")
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_broadcast_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST)
        .appendField(new Blockly.FieldTextInput("topic"), "topic");
    this.appendValueInput("group")
        .setCheck("Number")
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST_TO_GROUP);
    this.appendValueInput("msg")
        .setCheck(["Number", "String"])
        .appendField(Blockly.Msg.CPEKU_ESPNOW_BROADCAST_WITH_MSG);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_sender_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP);
    this.setOutput(true, "String");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_SENDER_GROUP_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_my_group'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_MY_GROUP);
    this.setOutput(true, "Number");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_MY_GROUP_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_my_addr'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_MY_ADDR);
    this.setOutput(true, "String");
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_MY_ADDR_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_set_group'] = {
  init: function() {
    this.appendValueInput("group")
        .setCheck("Number")
        .appendField(Blockly.Msg.CPEKU_ESPNOW_SET_GROUP);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
    this.setTooltip(Blockly.Msg.CPEKU_ESPNOW_SET_GROUP_TOOLTIP);
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_espnow_set_mode'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_ESPNOW_SET_MODE)
        .appendField(new Blockly.FieldDropdown([
          [Blockly.Msg.CPEKU_ESPNOW_MODE_STANDARD,"0"],
          [Blockly.Msg.CPEKU_ESPNOW_MODE_LR,"1"]
        ]), "mode");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(135);
 this.setTooltip("");
 this.setHelpUrl("");
  }
};
