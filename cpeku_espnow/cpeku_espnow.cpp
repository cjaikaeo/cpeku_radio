#include <string.h>
#include <map>

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp_log.h"
#include "tcpip_adapter.h"
#include "esp_event.h"
#include "tcpip_adapter.h"
#include "esp_wifi.h"
#include "esp_now.h"

#include "kidbright32.h"
#include "cpeku_espnow.h"

static const char *TAG = "CPEKU_EspNow";
static uint8_t BROADCAST_MAC[ESP_NOW_ETH_ALEN] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

CPEKU_EspNow::State CPEKU_EspNow::_state;
char CPEKU_EspNow::_received_text[256];
char CPEKU_EspNow::_my_mac_str[20];
char CPEKU_EspNow::_sender_mac_str[20];
uint16_t CPEKU_EspNow::_my_group;
uint16_t CPEKU_EspNow::_sender_group;
CPEKU_EspNow::CallbackMap CPEKU_EspNow::_cbmap;

CPEKU_EspNow::CPEKU_EspNow() {
  _state = DETECT;
  strcpy(_sender_mac_str, "00:00:00:00:00:00");
}

void CPEKU_EspNow::init(void) {
  _state = DETECT;
  esp_log_level_set(TAG,LOG_LOCAL_LEVEL);
}

void CPEKU_EspNow::process(Driver *drv) {
  switch (_state) {
    case DETECT:
      // clear error flag
      error = false;
      // set initialized flag
      initialized = true;
      _state = DISABLED;
      break;
    case DISABLED:
      break;
    case ENABLED:
      break;
  }
}

void CPEKU_EspNow::_recv_cb(const uint8_t *mac_addr, const uint8_t *data, int len) {
  MsgHeader *header = (MsgHeader*) data;

  if (header->dst_group != _my_group) return;

  const char* topic_ptr = (const char*)data + sizeof(MsgHeader);
  const char* msg_ptr = topic_ptr + header->topic_len;
  char topic_buf[256];
  memcpy(topic_buf, topic_ptr, header->topic_len);
  topic_buf[header->topic_len] = 0;

  ESP_LOGI(TAG, "receive %d bytes on topic '%s' from " MACSTR,
      header->msg_len, topic_buf, MAC2STR(mac_addr));

  // find the matched topic and pass the value to the corresponding callback
  auto it = _cbmap.find(topic_buf);
  if (it != _cbmap.end()) {
    ESP_LOGI(TAG, "callback for topic \"%s\" found",topic_buf);
    _sender_group = header->src_group;
    sprintf(_sender_mac_str, MACSTR, MAC2STR(mac_addr));
    memcpy(_received_text,msg_ptr,header->msg_len);
    _received_text[header->msg_len] = 0;
    it->second();
  }
  else {
    ESP_LOGI(TAG, "callback for topic \"%s\" not found",topic_buf);
  }
}

void CPEKU_EspNow::enable(uint8_t channel, uint16_t group) {
  ESP_LOGI(TAG, "enable ESP-NOW with channel %d and group %d", channel, group);

  /* WiFi should start before using ESPNOW */
  tcpip_adapter_init();
  //ESP_ERROR_CHECK(esp_event_loop_create_default());
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
  ESP_ERROR_CHECK( esp_wifi_start());
  ESP_ERROR_CHECK( esp_wifi_set_channel(channel, (wifi_second_chan_t)0) );

  uint8_t mac[ESP_NOW_ETH_ALEN];
  esp_wifi_get_mac(WIFI_IF_STA, mac);
  sprintf(_my_mac_str, MACSTR, MAC2STR(mac));
  _my_group = group;

  /* Initialize ESPNOW and register sending and receiving callback function. */
  ESP_ERROR_CHECK( esp_now_init() );
  ESP_ERROR_CHECK( esp_now_register_recv_cb(CPEKU_EspNow::_recv_cb) );

  /* Add broadcast peer information to peer list. */
  esp_now_peer_info_t *peer = (esp_now_peer_info_t*) malloc(sizeof(esp_now_peer_info_t));
  memset(peer, 0, sizeof(esp_now_peer_info_t));
  peer->channel = channel;
  peer->ifidx = WIFI_IF_STA;
  peer->encrypt = false;
  memcpy(peer->peer_addr, BROADCAST_MAC, ESP_NOW_ETH_ALEN);
  ESP_ERROR_CHECK( esp_now_add_peer(peer) );
  free(peer);

  _state = ENABLED;
}

void CPEKU_EspNow::on_message(const char* topic, OnMessageCallback cb) {
  ESP_LOGI(TAG, "Register a callback for topic '%s'", topic);
  _cbmap[topic] = cb;
}

double CPEKU_EspNow::received_number() {
  return strtod(_received_text,nullptr);
}

char* CPEKU_EspNow::received_text() {
  return _received_text;
}

char* CPEKU_EspNow::my_address() {
  return _my_mac_str;
}

uint16_t CPEKU_EspNow::my_group() {
  return _my_group;
}

char* CPEKU_EspNow::sender_address() {
  return _sender_mac_str;
}

uint16_t CPEKU_EspNow::sender_group() {
  return _sender_group;
}

void CPEKU_EspNow::broadcast(const char* topic, const char* msg, uint16_t dst_group) {
  uint8_t topic_len = strlen(topic);
  uint8_t msg_len = strlen(msg);
  uint8_t total_len = sizeof(MsgHeader) + topic_len + msg_len;

  if (total_len > ESP_NOW_MAX_DATA_LEN) {
    ESP_LOGI(TAG, "Message too long (%d bytes)", total_len);
    return;
  }

  char data[total_len];
  MsgHeader* header = (MsgHeader*)data;
  char* topic_ptr = data + sizeof(MsgHeader);
  char* msg_ptr = topic_ptr + topic_len;

  header->src_group = _my_group;
  header->dst_group = dst_group;
  header->topic_len = topic_len;
  header->msg_len = msg_len;
  memcpy(topic_ptr, topic, topic_len);
  memcpy(msg_ptr, msg, msg_len);

  if (esp_now_send(BROADCAST_MAC, (const uint8_t*)data, total_len) != ESP_OK) {
    ESP_LOGI(TAG, "Send error");
  }
  else {
    ESP_LOGI(TAG, "Send %d bytes to topic '%s' successfully", msg_len, topic);
  }
}

void CPEKU_EspNow::broadcast(const char* topic, const char* msg) {
  broadcast(topic, msg, _my_group);
}

void CPEKU_EspNow::broadcast(const char* topic, double num, uint16_t dst_group) {
	char buf[64];
	sprintf(buf, "%g", num);
  broadcast(topic, buf, dst_group);
}

void CPEKU_EspNow::broadcast(const char* topic, double num) {
  broadcast(topic, num, _my_group);
}

bool CPEKU_EspNow::is_enabled() {
  return _state == ENABLED;
}

void CPEKU_EspNow::set_group(uint16_t group) {
  ESP_LOGI(TAG, "set group to %d", group);
  _my_group = group;
}

void CPEKU_EspNow::set_longrange(uint8_t enabled) {
  if (enabled) {
    ESP_ERROR_CHECK( esp_wifi_set_protocol(WIFI_IF_STA, WIFI_PROTOCOL_LR) );
    ESP_LOGI(TAG, "long-range mode enabled");
  }
  else {
    ESP_ERROR_CHECK( esp_wifi_set_protocol(
          WIFI_IF_STA, WIFI_PROTOCOL_11B | WIFI_PROTOCOL_11G | WIFI_PROTOCOL_11N) );
    ESP_LOGI(TAG, "long-range mode disabled");
  }
}
